let nav = document.querySelector('nav');


function myFunction() {
    nav.classList.toggle("active");
    navig.classList.toggle("rotate");
}

function header_link(projectUrl){
    window.location.assign('/projects/' + projectUrl);
}

var load;

function loader() {
    if (document.readyState === 'complete') {
        showPage();
    }
}


function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("content").style.display = "block";
}

window.onload = function() {
    loader();
    sort();
};