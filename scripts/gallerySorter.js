
function sort() {
    if ($(document).width() < 720) return;
    var gWidth = $('#gallery').width(),
        counter = 0;


    $('.images').each(function (index) {
        counter++;
        var ratio = $(this).width() / $(this).height();

        console.log(index);

        if (index % 2 === 0) {
            var nextRatio = $(this).next().width() / $(this).next().height(),
                both = ratio + nextRatio,
                piece = gWidth / both;

            console.log($(this).width(), nextRatio, both, piece);

            $(this).width(ratio * piece - 5);
            $(this).next().width(nextRatio * piece - 5);

        }
    });

    if (counter%2==1) {
        $('.images').last().width(gWidth-5);
    }
}
