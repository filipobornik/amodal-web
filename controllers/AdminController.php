<?php

class AdminController extends Controller
{
    private $projectManager;
    private $loginManager;
    private $tagManager;
    private $imageManager;
    private $mediaManager;

    public function process($params)
    {
        session_start();
        $this->loginManager = new LoginManager();
        $this->projectManager = new ProjectManager();
        $this->tagManager = new TagManager();
        $this->imageManager = new ImageManager();
        $this->mediaManager = new MediaManager();

        if (!isset($params[0])) {
            $this->enterAdministration();
        } else {
            switch ($params[0]) {
                case 'add-tag':
                    $tagId = null;
                    if (isset($params[1])) $tagId = $params[1];
                    $this->handleTagAdding($tagId);
                    break;
                case 'add-project':
                    $projectId = null;
                    if (isset($params[1])) $projectId = $params[1];
                    $this->handleProjectAdding($projectId);
                    break;
                case 'add-description':
                    $projectId = $params[1];
                    $descriptionId = null;
                    if (isset($params[2])) $descriptionId = $params[2];
                    $this->handleDescriptionAdding($projectId, $descriptionId);
                    break;
                case 'add-media':
                    $projectId = $params[1];
                    $descriptionId = $params[2];
                    $mediaId = null;
                    if (isset($params[3])) $mediaId = $params[3];
                    $this->handleMediaAdding($projectId, $descriptionId, $mediaId);
                    break;
                case 'descriptions':
                    if (!isset($params[1])) $this->redirect('error');
                    $this->handleDescriptionsForProject($params[1]);
                    break;
                case 'description-media':
                    if (!isset($params[1]) || !isset($params[2])) $this->redirect('error');
                    $this->handleMedia($params[1], $params[2]);
                    break;
                case 'description-images':
                    $projectId = $params[1];
                    $descriptionId = $params[2];
                    $this->handleImages($projectId, $descriptionId);
                    break;
                case "add-image":
                    $projectId = $params[1];
                    $descriptionId = $params[2];
                    $this->handleImageAdding($projectId, $descriptionId);
                    break;
            }
        }
    }

    private function handleImageAdding($projectId, $idDescription) {
        if (isset($_FILES['image'])) {
            $result = $this->imageManager->saveImage($idDescription);
            $this->handleResult($result);
        }
        $this->data['projectId'] = $projectId;
        $this->data['descriptionId'] = $idDescription;
        $this->view = 'addImage';
    }

    private function handleImages($projectId, $descriptionId) {
        $this->data['projectId'] = $projectId;
        $this->data['description'] = $this->projectManager->getDescription($descriptionId);
        $this->data['images'] = $this->imageManager->getImagesForDescription($descriptionId);
        $this->view = 'descriptionImages';
    }

    private function handleMediaAdding($projectId, $idDescription, $idMedia) {
        if (isset($_POST['url'])) {

            if (strpos($_POST['url'], "src") !== false) {
                preg_match( '@src="([^"]+)"@' , $_POST['url'], $match);
                $src = array_pop($match);
                $_POST['url'] = $src;
            }

            if (!empty($_POST['mediaId'])) {
                $result = $this->mediaManager->editMedia((int)$_POST['mediaId'], $_POST['type'], $_POST['url']);
            } else {
                $result = $this->mediaManager->addMedia($_POST['url'], (int)$_POST['type'], (int)$idDescription);
            }
            $this->handleResult($result);
        }
        if ($idMedia != null) {
            $this->data['media'] = $this->mediaManager->getMedia((int)$idMedia);
        }
        $this->data['projectId'] = $projectId;
        $this->data['descriptionId'] = $idDescription;
        $this->view = 'addMedia';
    }

    private function handleMedia($projectId, $descriptionId) {
        $this->data['description'] = $this->projectManager->getDescription((int)$descriptionId);
        $this->data['projectId'] = $projectId;
        $this->data['project'] = $this->projectManager->getDescription((int)$descriptionId);
        $this->data['media'] = $this->mediaManager->getMediaForDescription((int)$descriptionId);
        $this->view = 'media';
    }

    private function handleDescriptionAdding($projectId, $descriptionId)
    {
        if (isset($_POST['title'])) {
            if (!empty($_POST['descriptionId'])) {
                $result = $this->projectManager->editDescription((int) $_POST['descriptionId'], (int) $projectId,
                    $_POST['title'], $_POST['client'], $_POST['objective'], $_POST['concept'], $_POST['date']);
            } else {
                $result = $this->projectManager->addProjectDescription((int) $projectId,
                    $_POST['title'], $_POST['client'], $_POST['objective'], $_POST['concept'], $_POST['date']);
                $descriptionId = $result;
            }
            $result += $this->tagManager->bindTagsToDescription($descriptionId, $_POST['tags']);
            $this->handleResult($result);
        }
        if ($descriptionId != null) {
            $this->data['description'] = $this->projectManager->getDescription((int)$descriptionId);
            $this->data['selectedTags'] = $this->tagManager->getTagsForDescription($descriptionId);
        }
        $this->data['tags'] = $this->tagManager->getAllTags();
        $this->data['projectId'] = $projectId;
        $this->view = 'addDescription';
    }

    private function handleDescriptionsForProject($projectId) {
        $project = $this->projectManager->getProjectDetail($projectId);
        $descriptions = $this->projectManager->getProjectDescriptions($projectId);

        $tagsToDescriptions = array();
        foreach ($descriptions as $description) {
            $tags = $this->tagManager->getTagsForDescription($description['description_id']);
            $tagsToDescriptions[$description['description_id']] = $tags;
        }

        $this->data['tags'] = $tagsToDescriptions;
        $this->data['descriptions'] = $descriptions;
        $this->data['project'] = $project;
        $this->view = 'descriptions';
    }

    private function handleTagAdding($tagId) {
        if (isset($_POST['tagName'])) {
            if (!empty($_POST['tagId'])) {
                $result = $this->tagManager->editTag($_POST['tagId'], $_POST['tagName']);
            } else {
                $result = $this->tagManager->addTag($_POST['tagName']);
            }
            $this->handleResult($result);
        }
        $this->data['tagId'] = $tagId;
        if ($tagId != null) $this->data['tagName'] = $this->tagManager->getTag($tagId)['tag_name'];
        $this->view = 'addTag';
    }

    private function handleProjectAdding($projectId) {
        if (isset($_POST['title'])) {
            $this->saveOrUpdateProject();
        }
        $this->data['projectId'] = $projectId;
        if ($projectId != null) {
            $projectDetail = $this->projectManager->getProjectDetail($projectId);
            $this->data['title'] = $projectDetail['title'];
            $this->data['mainImage'] = $this->imageManager->getImage($projectDetail['id_main_image']);
        }

        $this->view = 'addProject';
    }

    private function saveOrUpdateProject() {
        $imgId = null;
        if (isset($_FILES["mainImage"]["name"]) && !empty($_FILES["mainImage"]["name"])) {
            if (isset($_POST['projectId']) && !empty($_POST['projectId'])) {
                $project = $this->projectManager->getProjectDetail($_POST['projectId']);
                $image = $this->imageManager->getImage($project['id_main_image']);
//                unlink(dirname(__FILE__) . "/" . $image['path']);
                $this->imageManager->removeImage($project['id_main_image']);
            }
            $imgId = $this->imageManager->saveMainImage();
        }
        if (!empty($_POST['projectId'])) {
            $result = $this->projectManager->editProject($_POST['projectId'], $_POST['title'], (int)$imgId);
        } else {
            $result = $this->projectManager->addProject($_POST['title'], (int) $imgId);
        }
        $this->handleResult($result);
    }

    private function enterAdministration() {
        if ($this->loginUser()) {
            $this->setDataForAdministration();
            $this->view = 'administration';
        } else {
            $this->view = 'login';
        }
    }

    private function loginUser() {
        if ($this->loginManager->isUserLoggedIn()) {
            return true;
        }

        if (empty($_POST['username']) || empty($_POST['password'])) {
            return false;
        }

        if ($this->loginManager->login(htmlspecialchars($_POST['username']), htmlspecialchars($_POST['password']))) {
            $this->loginManager->createSession($_POST['username']);
            $this->redirect('admin');
            return true;
        } else {
            return false;
        }
    }

    private function setDataForAdministration() {
        $this->data['projects'] = $this->projectManager->getAllProjects();
        $this->data['tags'] = $this->tagManager->getAllTags();
    }

    private function handleResult($result) {
        if ($result <= 0) {
            echo '<p class="error">Changes not saved</p>';
        } else {
            echo '<p class="success">Changes successfully saved</p>';
        }
    }
}