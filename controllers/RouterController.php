<?php

class RouterController extends Controller
{

    protected $controller;

    public function process($params)
    {
        $parsedUrl = $this->parseUrl($params[0]);
        $this->view = 'layout';

        if (empty($parsedUrl[0])) {
            $this->redirect("main");
        }

        $controllerName = $this->dashesToCammelCase(array_shift($parsedUrl)) . 'Controller';

        if (file_exists("controllers/$controllerName.php")) {
            $this->controller = new $controllerName;
        } else {
            $this->redirect('error');
        }

        $this->controller->process($parsedUrl);

        $this->data['title'] = $this->controller->header['title'];
        $this->data['description'] = $this->controller->header['description'];
        $this->data['keywords'] = $this->controller->header['keywords'];
    }

    private function parseUrl($url) {
        $parsedUrl = parse_url($url);
        $parsedUrl['path'] = ltrim($parsedUrl['path'], '/');
        $parsedUrl['path'] = trim($parsedUrl['path']);

        $splittedPath = explode('/', $parsedUrl['path']);
        return $splittedPath;
    }

    private function dashesToCammelCase($text) {
        $name = str_replace('-', ' ', $text);
        $name = ucwords($name);
        $name = str_replace(' ', '', $name);
        return $name;
    }
}