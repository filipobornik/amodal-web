<?php

class MainController extends Controller
{

    private $projectManager;
    private $imageManager;

    public function process($params)
    {
        $this->projectManager = new ProjectManager();
        $this->imageManager = new ImageManager();
        $randomProject = $this->projectManager->getRandomProject();
        header("HTTP/1.0 404 Not Found");
        $this->header['title'] = 'Amodal | Creative studio';
        $this->view = 'error';
        $this->data['project'] = $randomProject;
        $this->data['path'] = $this->imageManager->getImage($randomProject['id_main_image'])['path'];
        $this->view = 'main';
    }
}