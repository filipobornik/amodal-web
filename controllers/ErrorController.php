<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 20/10/2018
 * Time: 17:00
 */

class ErrorController extends Controller
{

    public function process($params)
    {
        header("HTTP/1.0 404 Not Found");
        $this->header['title'] = 'Error 404';
        $this->view = 'error';
    }
}