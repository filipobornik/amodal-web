<?php

class RemoveDescriptionController extends Controller
{

    private $projectManager;
    private $loginManager;
    private $mediaManager;
    private $tagManager;
    private $imageManager;

    public function process($params)
    {
        session_start();
        $this->loginManager = new LoginManager();
        $this->projectManager = new ProjectManager();
        $this->mediaManager = new MediaManager();
        $this->tagManager = new TagManager();
        $this->imageManager = new ImageManager();

        if (!$this->loginManager->isUserLoggedIn()) {
            $this->redirect('admin');
        }

        $description = $this->projectManager->getDescription($params[1]);
        $descriptionMedia = $this->mediaManager->getMediaForDescription($description['descriptionId']);

        foreach ($descriptionMedia as $media) {
            $this->mediaManager->removeMedia($media['id']);
        }

        $images = $this->imageManager->getImagesForDescription($description['descriptionId']);
        foreach ($images as $image) {
            unlink(dirname(__FILE__) . "/" . $image['path']);
            $this->imageManager->removeImage($image['id']);
        }

        $this->tagManager->removeTagsForDescription($params[1]);
        $this->projectManager->removeDescription($params[1]);
        $this->redirect('admin/descriptions/' . $params[0]);
        echo "removed";
    }
}