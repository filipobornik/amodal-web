<?php

class RemoveImageController extends Controller
{

    private $imageManager;
    private $loginManager;

    public function process($params)
    {
        session_start();
        $this->loginManager = new LoginManager();
        $this->imageManager = new ImageManager();

        if (!$this->loginManager->isUserLoggedIn()) {
            $this->redirect('admin');
        }
        $image = $this->imageManager->getImage($params[0]);
        unlink(dirname(__FILE__) . "/" . $image['path']);
        $this->imageManager->removeImage($params[2]);
        $this->redirect('admin/description-images/' . $params[0] . '/' . $params[1]);
        echo "removed";
    }

}