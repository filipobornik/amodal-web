<?php

class ContactController extends Controller
{

    public function process($params)
    {
        $this->header['title'] = "Contactos";
        $this->view = 'contact';
    }
}