<?php
/**
 * Created by PhpStorm.
 * User: filip
 * Date: 18/10/2018
 * Time: 18:17
 */

abstract class Controller
{

    protected $data = array();
    protected $view = "";
    protected $header = array('title' => '', 'keywords' => '', 'description' => '');

    public abstract function process($params);

    public function printView() {
        if ($this->view) {
            extract($this->data);
            require("views/$this->view.phtml");
        }
    }

    public function redirect($url) {
        header("Location: /$url");
        header("Connection: close");
        exit;
    }

}