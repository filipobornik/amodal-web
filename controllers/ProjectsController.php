<?php

class ProjectsController extends Controller
{

    private $projectManager;
    private $resourcesManager;
    private $imageManager;
    private $tagManager;
    private $mediaManager;

    public function process($params)
    {
        $this->mediaManager = new MediaManager();
        $this->projectManager = new ProjectManager();
        $this->resourcesManager = new ResourceManager();
        $this->tagManager = new TagManager();
        $this->imageManager = new ImageManager();

        if (!$params) {
            $this->printAllProjects();
        } else {
            $this->printProjectDetail($params[0]);
        }
    }

    private function printAllProjects()
    {
        if (isset($_GET['search']) && !empty($_GET['search'])) {
            $search = htmlspecialchars($_GET['search']);
            $projects = $this->projectManager->searchProjects($search);
        } else {
            $projects = $this->projectManager->getAllProjects();
        }
        $mainImages = array();

        foreach ($projects as $project) {
            $mainImages[$project['id']] = $this->imageManager->getImage($project['id_main_image']);
        }

        $this->addHeaderForAllProject();
        $this->data['projects'] = $projects;
        $this->data['mainImages'] = $mainImages;

        $this->view = 'projects';
    }

    private function printProjectDetail($projectUrl)
    {
        $project = $this->projectManager->getProjectDetailByUrl($projectUrl);
        $descriptions = $this->projectManager->getProjectDescriptions($project['id']);
        $this->exitIfDoNotExist($descriptions);

        $images = $this->getImagesForDescriptions($descriptions);
        $tags = $this->getTagsForDescriptions($descriptions);
        $media = $this->getMediaForDescriptions($descriptions);

        $this->header['title'] = $project['title'];
        $this->data['media'] = $media;
        $this->data['title'] = $project['title'];
        $this->data['descriptions'] = $descriptions;
        $this->data['images'] = $images;
        $this->data['tags'] = $tags;
        $this->data['anotherProjects'] = $this->projectManager->getThreeRandomProjects();
        $this->data['anotherProjectsImages'] = $this->imageManager->getImagesForProjects($this->data['anotherProjects']);

        $this->view = 'projectDetail';
    }

    private function addHeaderForAllProject()
    {
        $this->header['title'] = 'All projects';
        $this->view = 'error';
    }

    private function getImagesForDescriptions($descriptions) {
        $imagesForDescriptions = array();
        foreach ($descriptions as $description) {
            $images = $this->resourcesManager->getImagesForDescription($description['description_id']);
            $imagesForDescriptions[$description['description_id']] = $images;
        }
        return $imagesForDescriptions;
    }

    private function getMediaForDescriptions($descriptions) {
        $mediaForDescriptions = array();
        foreach ($descriptions as $description) {
            $media = $this->mediaManager->getMediaForDescription($description['description_id']);
            $mediaForDescriptions[$description['description_id']] = $media;
        }
        return $mediaForDescriptions;
    }

    private function getTagsForDescriptions($descriptions) {
        $tagsForDescription = array();
        foreach ($descriptions as $description) {
            $tags = $this->tagManager->getTagsForDescription($description['description_id']);
            $tagsForDescription[$description['description_id']] = $tags;
        }
        return $tagsForDescription;
    }

    private function exitIfDoNotExist($value)
    {
        if (!$value) {
            $this->redirect('error');
            exit();
        }
    }

}