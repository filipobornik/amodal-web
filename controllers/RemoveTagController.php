<?php

class RemoveTagController extends Controller
{

    private $projectManager;
    private $loginManager;
    private $tagManager;

    public function process($params)
    {
        session_start();
        $this->loginManager = new LoginManager();
        $this->projectManager = new ProjectManager();
        $this->tagManager = new TagManager();

        if (!$this->loginManager->isUserLoggedIn()) {
            $this->redirect('admin');
        }

        $this->tagManager->removeTag((int)$params[0]);
        $this->redirect("admin");
    }

}