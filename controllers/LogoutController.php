<?php

class LogoutController extends Controller
{

    public function process($params)
    {
        $loginManager = new LoginManager();
        $loginManager->logoutUser();
        $this->redirect('admin');
    }

}