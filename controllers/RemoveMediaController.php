<?php

class RemoveMediaController extends Controller
{

    private $mediaManager;
    private $loginManager;

    public function process($params)
    {
        session_start();
        $this->loginManager = new LoginManager();
        $this->mediaManager = new MediaManager();

        if (!$this->loginManager->isUserLoggedIn()) {
            $this->redirect('admin');
        }

        $this->mediaManager->removeMedia($params[2]);
        $this->redirect('admin/description-media/' . $params[0] . '/' . $params[1]);
        echo "removed";
    }

}