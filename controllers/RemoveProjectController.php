<?php

class RemoveProjectController extends Controller
{

    private $projectManager;
    private $loginManager;
    private $mediaManager;
    private $tagManager;
    private $imageManager;

    public function process($params)
    {
        session_start();
        $this->loginManager = new LoginManager();
        $this->projectManager = new ProjectManager();
        $this->mediaManager = new MediaManager();
        $this->tagManager = new TagManager();
        $this->imageManager = new ImageManager();

        if (!$this->loginManager->isUserLoggedIn()) {
            $this->redirect('admin');
        }

        $projectId = $params[0];

        $descriptions = $this->projectManager->getProjectDescriptions($projectId);
        foreach ($descriptions as $description) {
            $descriptionMedia = $this->mediaManager->getMediaForDescription($description['description_id']);
            foreach ($descriptionMedia as $media) {
                $this->mediaManager->removeMedia($media['id']);
            }
            $this->tagManager->removeTagsForDescription($description['description_id']);

            $images = $this->imageManager->getImagesForDescription($description['description_id']);

            foreach ($images as $image) {
                unlink(dirname(__FILE__) . "/" . $image['path']);
                $this->imageManager->removeImage($image['imageId']);
            }
            $this->projectManager->removeDescription($description['description_id']);

        }
        $this->projectManager->removeProject($projectId);

        $this->redirect('admin');
        echo "removed";
    
    }
}