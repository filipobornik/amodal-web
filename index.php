<?php
mb_internal_encoding("UTF-8");
spl_autoload_register("autoload");

Database::connect('localhost', 'root', 'password', 'amodal');

$router = new RouterController();
$router->process(array($_SERVER['REQUEST_URI']));
$router->printView();

function autoload($class)
{
    if (preg_match('/Controller$/', $class))
        require("controllers/" . $class . ".php");
    else
        require("models/" . $class . ".php");
}