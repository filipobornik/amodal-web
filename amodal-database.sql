-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 26, 2018 at 10:19 AM
-- Server version: 8.0.12
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE Database amodal character set utf8;
USE amodal;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amodal`
--

-- --------------------------------------------------------

--
-- Table structure for table `description`
--

CREATE TABLE `description` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `client` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `objective` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `concept` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` date NOT NULL,
  `id_project` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `description`
--

INSERT INTO `description` (`id`, `title`, `client`, `objective`, `concept`, `date`, `id_project`) VALUES
(1, 'Arquitectura Efímera', 'MuchoMaskemarket es un evento que organiza la asociación ADMIA, autofinanciado y sin ánimo de lucro. El objetivo del MMM es visibilizar y poner en valor el trabajo de diferentes profesionales del Art & Crafts de Andalucía.', 'Elegir localización y propuesta arquitectónica para la edición de MMM 2014.', 'Para el encuentro entre diseñadores, creadores y público se propone un espacio diáfano en el casco histórico de la ciudad donde poder desarrollar el concepto artesanal del propio evento. El local elegido está a 60 m de la puerta trasera del mercado de calle Feria. Una localización clave para articular la analogía entre un mercado de abastos tradicional del  S. XVIII y un mercado artesanal del S. XXI.\r\n\r\nPara la escenografía proponemos un sistema modular de andamios, neutro aunque con personalidad, con capacidad de crecer o decrecer en cada edición y con posibilidad de alquiler internacional. ', '2014-01-01', 2),
(2, 'Diseño', 'MuchoMaskemarket es un evento que organiza la asociación ADMIA, autofinanciado y sin ánimo de lucro. El objetivo del MMM es visibilizar y poner en valor el trabajo de diferentes profesionales del Art & Crafts de Andalucía. ', 'Realizar cartel y diseños adaptados para flyer y redes sociales.', 'Representar de manera gráfica los conceptos Navidad y Artesanía. ', '2015-01-01', 2),
(3, 'Fotografía & Dirección de arte', 'María de Gracia es una joven diseñadora de Carmona (Sevilla) con una firma en activo desde 2010.', 'Realizar una sesión de fotos para su colección Mujer de armas tomar. ', 'Mujer de armas tomar se abordó desde la visión tan certera y polémica de John Berger (Londres, 1926):\r\n\r\n“Los hombres miran a las mujeres. Las mujeres se contemplan a sí mismas siendo miradas. Una mujer debe contemplarse continuamente. Ha de ir acompañada casi constantemente por la imagen que tiene de sí misma. Cuando cruza una habitación o llora por la muerte de su padre, a duras penas evita imaginarse a sí misma caminando o llorando. Desde su más temprana infancia se le ha enseñado a examinarse continuamente. Y así llega a considerar que la examinante y la examinada que hay en ella son dos elementos constituyentes, pero siempre distintos, de su identidad como mujer”.\r\n\r\nModelo: Rocío García Sánchez (Sevilla, 1997)', '2014-12-01', 3),
(4, 'MARÍA DE GRACIA 2015 Música', 'María de Gracia es una joven diseñadora de Carmona (Sevilla) con una firma en activo desde 2010.', 'Diseñar un mix para mezclarlo en la presentación de la colección en directo. Era la segunda vez que María de Gracia vestía la pasarela de SIMOF y para la edición de 2015 no bastaba con una colección sólida, necesitaba un espectáculo serio pero transgresor con la atmósfera ritualística que emana del flamenco. ', 'Techno lento y flamenco. Un sonido profundo no agresivo que acompañara con firmeza el paso lento y voluptuoso de las modelos. 90 golpes por minuto (90 bpm) y las voces desgarradas por martinetes de Diego El Cigala, Esperanza Fernández, Pepe el Culata y Enrique Morente.', '2015-02-01', 3),
(5, 'MARÍA DE GRACIA 2016', 'María de Gracia es una diseñadora de Carmona (Sevilla) con una firma en activo desde 2010.', 'Proponer un show, transgresor, con la identidad de María de Gracia. ', 'María de Gracia usó en su colección un moderno tejido industrial presente en el mundo del tunning. Las voces que escuchamos por encima de la capa techno son martinetes acompañados ocasionalmente de un yunque. El yunque es un elemento primitivo de lo industrial. Las letras tienen contenido triste y tono monocorde, terminando en largos quejíos. El sonido techno viste la experiencia con aire bruto, espeso y lento a 90 beats por minuto.', '2016-02-01', 3),
(6, 'Alfaluz', '', 'Mostrar el sistema petrolífero de la compañía. ', 'Animación para narrar visualmente un proceso técnico.', '2015-08-01', 6),
(7, 'MARÍA DE GRACIA 2014 Fotografía', 'María de Gracia es una joven diseñadora de Carmona (Sevilla) con una firma en activo desde 2010.', 'Realizar una sesión de fotos para su colección Mujer de armas tomar. ', 'Mujer de armas tomar se abordó desde la visión tan certera y polémica de John Berger (Londres, 1926):\r\n\r\n“Los hombres miran a las mujeres. Las mujeres se contemplan a sí mismas siendo miradas. Una mujer debe contemplarse continuamente. Ha de ir acompañada casi constantemente por la imagen que tiene de sí misma. Cuando cruza una habitación o llora por la muerte de su padre, a duras penas evita imaginarse a sí misma caminando o llorando. Desde su más temprana infancia se le ha enseñado a examinarse continuamente. Y así llega a considerar que la examinante y la examinada que hay en ella son dos elementos constituyentes, pero siempre distintos, de su identidad como mujer”.\r\n\r\nModelo: Rocío García Sánchez (Sevilla, 1997)', '2014-12-01', 3),
(8, 'FARMACIA METRO', 'Marisa Aguilera es una persona que se ha hecho a sí misma; el trabajo y su sonrisa la acompañan siempre. Bastan quince minutos en la Farmacia Metro para darse cuenta de que no es una farmacia globalizada; personas de todas las edades se sienten como en casa. ', 'Renovar la imagen de la farmacia. Una farmacia por y para su gente que aunara servicios de vanguardia con el ambiente más familiar y cercano. \r\n', 'La Farmacia Metro está a cinco minutos caminando de la parada de metro San Juan Bajo (Sevilla), al lado de una carretera con mucho tránsito. ', '2015-10-01', 7),
(9, 'DOHO', 'Doho es el proyecto empresarial de Alfredo Villarrubia. Una empresa que realiza servicios de mantenimiento y reparación a viviendas de lujo en La Zagaleta, uno de los más prestigiosos clubs de campo de Europa. Una urbanización privada de cerca de 900 hectáreas situada en plena Serranía de Ronda, a unos kilómetros de Marbella y a apenas 60 km del aeropuerto de Málaga.\r\nDoho ofrece servicios de mantenimiento en áreas como climatización, fontanería, electricidad, iluminación, domótica, tratamiento de aguas, piscinas, saunas', 'Crear una marca sofisticada como si se tratara de una marca de lujo a la que acostumbran sus clientes. Afianzar y reforzar su imagen. La idea principal es que gestores de la propiedad y clientes finales perciban profesionalidad, seguridad, lujo y buen gusto.', 'Presentar a Alfredo como el doctor de las casas. The doctor of houses. DO HO.', '2015-10-01', 8),
(10, 'BABILA', 'Juan Pablo y David, los propietarios de la tienda, tuvieron que reinventarse tras la crisis y decidieron montar una tienda de zapatillas, un hobbie que les apasionaba.', 'Crear una concept store, es decir, una tienda con una propuesta de valor única, concreta y diferente que solucione a su vez un reto de almacenamiento: guardar 750 cajas en un local de 27m2.', 'Hacer del problema la solución. \r\nEl mecanismo es como una noria que mantiene este pequeño espacio de 27 m² diáfano y permite almacenar 750 pares entre suelo y techo. \r\nPiñones, cadenas y guías conforman una estructura similar a una pequeña atracción de feria. El problema de almacenamiento convertido en la identidad del espacio.\r\nUn software de arduino conectado al mecanismo controla el stockage con una aplicación que permite  vivir una experiencia de compra única, virtual y física.', '2015-11-01', 9),
(11, 'Media test', 'Nulla quis diam. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Fusce suscipit libero eget elit. Aenean id metus id velit ullamcorper pulvinar. Etiam commodo dui eget wisi. Fusce wisi. Cras elementum. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Curabitur bibendum justo non orci. Excepteur sint occaecat cupidatat.', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Proin pede metus, vulputate nec, fermentum fringilla, vehicula vitae, justo. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Etiam dui sem, fermentum vitae.', 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Ut enim ad minim veniam.', '2018-10-11', 10);

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `id_description` int(11) DEFAULT NULL,
  `img_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `path`, `id_description`, `img_description`) VALUES
(22, 'images/project_images/MMM01.png', 1, 'estante'),
(23, 'images/project_images/MMM02.png', 1, 'estante'),
(24, 'images/project_images/MMM03.png', 1, 'photo'),
(25, 'images/project_images/MMM04.png', 1, 'photo'),
(26, 'images/project_images/MMM05.png', 1, 'photo'),
(27, 'images/project_images/MMM06.png', 1, 'plan'),
(28, 'images/project_images/MMM07.png', 1, 'photo'),
(29, 'images/project_images/MMM08.png', 1, 'render'),
(30, 'images/project_images/MMM09.png', 1, 'render'),
(31, 'images/project_images/MMM10.png', 1, 'photo'),
(33, 'images/project_images/MMM12.png', 2, 'cartel'),
(34, 'images/project_images/ALFALUZ01.png', 6, 'render'),
(35, 'images/project_images/GRACIA01.png', 7, 'vestido'),
(36, 'images/project_images/GRACIA02.png', 7, 'vestido'),
(37, 'images/project_images/GRACIA03.png', 7, 'vestido'),
(38, 'images/project_images/GRACIA04.png', 7, 'vestido'),
(39, 'images/project_images/GRACIA05.png', 7, 'vestido'),
(40, 'images/project_images/GRACIA06.png', 7, 'vestido'),
(41, 'images/project_images/GRACIA07.png', 7, 'vestido'),
(42, 'images/project_images/GRACIA08.png', 7, 'vestido'),
(43, 'images/project_images/GRACIA09.png', 7, 'vestido'),
(44, 'images/project_images/GRACIA10.png', 7, 'vestido'),
(45, 'images/project_images/GRACIA11.png', 7, 'vestido'),
(46, 'images/project_images/GRACIA11.png', 5, 'vestido'),
(47, 'images/project_images/GRACIA11.png', 5, 'vestido'),
(48, 'images/project_images/GRACIA11.png', 5, 'vestido'),
(49, 'images/project_images/GRACIA11.png', 5, 'vestido'),
(50, 'images/project_images/main/1540458148931-FM05.png', NULL, 'Realización'),
(51, 'images/project_images/1540458448028-FM01.png', 8, 'Fasade'),
(52, 'images/project_images/1540458453731-FM02.png', 8, 'Fasade'),
(53, 'images/project_images/1540458458751-FM03.png', 8, 'Fasade'),
(54, 'images/project_images/1540458466863-FM04.png', 8, 'Fasade'),
(55, 'images/project_images/1540458480126-FM05.png', 8, 'Realización'),
(56, 'images/project_images/main/1540459351432-DH04.png', NULL, 'utilidad'),
(57, 'images/project_images/1540459605650-DH01.png', 9, 'utilidad'),
(58, 'images/project_images/1540459626592-DH02.png', 9, 'auto'),
(59, 'images/project_images/1540459631431-DH03.png', 9, 'utilidad'),
(60, 'images/project_images/1540459635498-DH04.png', 9, 'utilidad'),
(61, 'images/project_images/1540459641499-DH05.png', 9, 'utilidad'),
(62, 'images/project_images/1540459659916-DH06.png', 9, 'ver'),
(63, 'images/project_images/1540459664842-DH07.png', 9, 'utilidad'),
(65, 'images/project_images/main/1540460834052-BA09.png', NULL, 'render'),
(66, 'images/project_images/main/1540464457548-bottle-1024.jpg', NULL, 'Test');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `url` text CHARACTER SET utf8 COLLATE utf8_bin,
  `id_description` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `url`, `id_description`, `type`) VALUES
(11, 'https://www.youtube.com/embed/LmbLiTdTEFk', 11, 0),
(12, 'https://player.vimeo.com/video/136920991', 11, 0),
(14, 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/512883342&color=%230d0d0d&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true', 11, 2),
(15, 'https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/337295747&color=%230d0d0d&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true', 9, 2),
(16, 'https://www.youtube.com/embed/2QJ2L2ip32w', 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `id_main_image` int(11) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `title`, `id_main_image`, `url`) VALUES
(2, 'Muchomaskemarket', 22, 'muchomaskemarket'),
(3, 'MARÍA DE GRACIA', 45, 'maria-de-gracia'),
(6, 'Alfaluz', 34, 'alfaluz'),
(7, 'FARMACIA METRO', 50, 'FARMACIA-METRO'),
(8, 'DOHO', 56, 'DOHO'),
(9, 'BABILA', 65, 'BABILA'),
(10, 'Test', 66, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `projectdescription_tags`
--

CREATE TABLE `projectdescription_tags` (
  `id_description` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Dumping data for table `projectdescription_tags`
--

INSERT INTO `projectdescription_tags` (`id_description`, `id_tag`) VALUES
(2, 7),
(6, 10),
(6, 2),
(6, 3),
(7, 4),
(7, 5),
(4, 13),
(5, 13),
(5, 5),
(5, 10),
(5, 14),
(8, 8),
(8, 11),
(9, 4),
(9, 8),
(9, 9),
(9, 10),
(9, 11),
(10, 1),
(10, 10),
(10, 15),
(1, 3),
(1, 6),
(1, 9),
(11, 1),
(11, 3),
(11, 4),
(11, 5),
(11, 6),
(11, 7),
(11, 8),
(11, 9),
(11, 10);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `id` int(11) NOT NULL,
  `tag_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`id`, `tag_name`) VALUES
(1, 'Arquitectura'),
(2, 'Render'),
(3, 'Animación'),
(4, 'Fotografía'),
(5, 'Dirección de arte'),
(6, 'Arquitectura Efímera'),
(7, 'Diseño Cartel'),
(8, 'Branding'),
(9, 'Web'),
(10, 'Vídeo'),
(11, 'Identidad Visual'),
(12, 'Diseño'),
(13, 'Música'),
(14, 'Escenografía'),
(15, 'Diseño Interior');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `admin`) VALUES
(2, 'amodalAdmin', '13e79e1ab48bc7298b2ddbfffc07a28cb2ab7e1f2e1e1bdd7214b972b1defe47', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `description`
--
ALTER TABLE `description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `description_project_id_fk` (`id_project`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_description_id_fk` (`id_description`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `music_id_uindex` (`id`),
  ADD KEY `music_description_id_fk` (`id_description`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projectdescription_tags`
--
ALTER TABLE `projectdescription_tags`
  ADD KEY `projectDescription_tags_tag_id_fk` (`id_tag`),
  ADD KEY `projectDescription_tags_description_id_fk` (`id_description`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_uindex` (`id`),
  ADD UNIQUE KEY `user_username_uindex` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `description`
--
ALTER TABLE `description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `description`
--
ALTER TABLE `description`
  ADD CONSTRAINT `description_project_id_fk` FOREIGN KEY (`id_project`) REFERENCES `project` (`id`);

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `image_description_id_fk` FOREIGN KEY (`id_description`) REFERENCES `description` (`id`);

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `music_description_id_fk` FOREIGN KEY (`id_description`) REFERENCES `description` (`id`);

--
-- Constraints for table `projectdescription_tags`
--
ALTER TABLE `projectdescription_tags`
  ADD CONSTRAINT `projectDescription_tags_description_id_fk` FOREIGN KEY (`id_description`) REFERENCES `description` (`id`),
  ADD CONSTRAINT `projectDescription_tags_tag_id_fk` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
