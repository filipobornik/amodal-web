{\rtf1\ansi\ansicpg1252\cocoartf1671
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\paperw11900\paperh16840\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0\fs24 \cf0 Add all files to webhosting (do not forget .htaccess - it is really important)\
Import database from file \'91amodal-database.sql\'92\
In \'91index.php\'92 change database credentials\
In \'91layout.phtml\'92 change \'91<base href="/localhost/" />\'92 to \'91<base href=\'93\{yourHostingUrl\}\'93 />\'92 \
\
Link for github repository: https://gitlab.com/filipobornik/amodal-web.git\
My email adress: filipobornik@gmail.com\
\
Contact me if you need some help with website.}