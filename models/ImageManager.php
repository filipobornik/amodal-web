<?php

class ImageManager
{

    public function saveMainImage() {
        $fileName = round(microtime(true) * 1000) . "-" . $this->getServerAcceptibleFileName($_FILES['mainImage']['name']);
        $fileTmpName  = $_FILES['mainImage']['tmp_name'];

        $currentDir = __DIR__ . '/../';
        $targetDir = "images/project_images/main/";
        $targetFile = $currentDir . $targetDir . basename($fileName);

        if (move_uploaded_file($fileTmpName, $targetFile)) {
            echo "The file ". basename($_FILES["mainImage"]["name"]). " has been uploaded.";
            $insertedId = Database::queryWithInsertedRowId("
              INSERT INTO image (path, img_description)
              VALUES (?, ?)", array($targetDir . basename($fileName), $_POST['imgDescription']));
            return $insertedId;
        } else {
            echo "Sorry, there was an error uploading your file.";
            return -1;
        }
    }

    public function saveImage($idDescription) {
        $fileName = round(microtime(true) * 1000) . "-" . $this->getServerAcceptibleFileName($_FILES['image']['name']);
        $fileTmpName  = $_FILES['image']['tmp_name'];

        $currentDir = __DIR__ . '/../';
        $targetDir = "images/project_images/";
        $targetFile = $currentDir . $targetDir . basename($fileName);

        if (move_uploaded_file($fileTmpName, $targetFile)) {
            echo "The file ". basename($_FILES["image"]["name"]). " has been uploaded.";
            $insertedId = Database::queryWithInsertedRowId("
              INSERT INTO image (path, img_description, image.id_description)
              VALUES (?, ?, ?)", array($targetDir . basename($fileName), $_POST['imgDescription'], (int)$idDescription));
            return $insertedId;
        } else {
            echo "Sorry, there was an error uploading your file.";
            return -1;
        }
    }

    public function addImage($path, $img_description, $descriptionId) {
        return Database::queryWithInsertedRowId("
        INSERT INTO image (image.path, image.img_description, image.id_description)
        values (?,?,?)
        ", array($path, $img_description, $descriptionId));
    }

    public function getImagesForProjects($projects) {
        $images = array();
        foreach ($projects as $project) {
            $images[$project['id']] = $this->getImage($project['id_main_image']);
        }
        return $images;
    }

    public function getImage($imageId) {
        return Database::getFirstResult("
              SELECT id, img_description, path, id_description
              FROM image
              where id = ?",
            array($imageId));
    }

    public function getImagesForDescription($descriptionId) {
        return Database::getAllResults("
              SELECT description.id as descriptionId, image.id as imageId, image.path, image.img_description
              FROM image
              join description on image.id_description = description.id
              where description.id = ?",
            array($descriptionId));
    }

    public function removeImage($imageId) {
        return Database::queryWithAffectedRows(
            "DELETE FROM image WHERE image.id = ?",
            array($imageId));
    }

    private function getServerAcceptibleFileName($baseName) {
        $fileName = str_replace(' ', '-', $baseName);
        $fileName = iconv('UTF-8', 'ASCII//TRANSLIT', $fileName);
        return $fileName;
    }

}