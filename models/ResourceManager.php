<?php

class ResourceManager
{

    public function getImagesForDescription($descriptionId)
    {
        return Database::getAllResults("
            SELECT description.id as `description_id`, image.id as `image_id`, image.path, image.img_description
            FROM description
            JOIN image ON description.id = image.id_description
            WHERE description.id = ?",
            array($descriptionId));
    }

}