<?php

class ProjectManager
{

    public function getThreeRandomProjects()
    {
        return Database::getAllResults("
         SELECT `id`, `title`, `id_main_image`, `url`
                        FROM `project` ORDER BY RAND() LIMIT 3", array());
    }

    public function getTagFrequencyForAllProjects()
    {
        return Database::getAllResults("select project.id, count(t.id) as tag_count, t.id as tag_id
        from project
               join description d on project.id = d.id_project
               join projectDescription_tags t2 on d.id = t2.id_description
               join tag t on t2.id_tag = t.id
        group by project.id , t.id;", array());
    }

    public function getTagFrequencyForProject($projectId)
    {
        return Database::getAllResults("select project.id, count(t.id) as tag_count, t.id as tag_id
        from project
               join description d on project.id = d.id_project
               join projectDescription_tags t2 on d.id = t2.id_description
               join tag t on t2.id_tag = t.id
               where project.id = ?
        group by project.id , t.id;", array($projectId));
    }

    public function getProjectDetailByUrl($url)
    {
        return Database::getFirstResult('
                        SELECT `id`, `title`, `id_main_image`, `url`
                        FROM `project`
                        WHERE `url` = ?',
            array($url));
    }

    public function searchProjects($query)
    {
        $projects = Database::getAllResults("
              select project.id, project.url, project.title, project.id_main_image
              from project
              join description d on project.id = d.id_project
              join projectDescription_tags t2 on d.id = t2.id_description
              join tag t on t2.id_tag = t.id
              where LOWER(tag_name) = LOWER(?)
              group by project.id;",
            array($query));

        $simTitleProjects = Database::getAllResults("
                select project.id, project.title, project.id_main_image, project.url
                from project
                where lower(project.title) LIKE lower('%$query%')",
            array());
        $projects = array_merge($projects, $simTitleProjects);

        return $projects;
    }


    public function getProjectDetail($projectId)
    {
        return Database::getFirstResult('
                        SELECT `id`, `title`, `id_main_image`, `url`
                        FROM `project`
                        WHERE `id` = ?',
            array($projectId));
    }

    public function getProjectDescriptions($projectId)
    {
        return Database::getAllResults('
                        SELECT project.id as `project_id`, description.id as `description_id`, description.title, description.client, description.objective, description.concept, description.date
                        FROM project
                        JOIN description ON project.id = description.id_project
                        WHERE project.id = ?
                        order by description.date DESC',
            array($projectId));
    }

    public function getDescription($descriptionId)
    {
        return Database::getFirstResult('
                SELECT description.id as descriptionId, description.title, description.client, description.objective, description.concept, description.date, description.id_project
                FROM description
                WHERE description.id = ?',
            array($descriptionId));
    }

    public function addProjectDescription($projectId, $title, $client, $objective, $concept, $date)
    {
        return Database::queryWithInsertedRowId("
        INSERT INTO description(description.title, description.id_project, description.objective, description.concept,
        description.client, description.date)
        VALUES (?, ?, ?, ?, ?, ?);
        ", array($title, $projectId, $objective, $concept, $client, $date));
    }

    public function editDescription($descriptionId, $projectId, $title, $client, $objective, $concept, $date)
    {
        return Database::queryWithAffectedRows("
                UPDATE description
                SET description.id_project = ?, description.title = ?, description.client = ?,
                description.objective = ?, description.concept = ?, description.date = ?
                WHERE description.id = ?
        ", array($projectId, $title, $client, $objective, $concept, $date, $descriptionId));
    }

    public function removeDescription($descriptionId)
    {
        return Database::queryWithAffectedRows(
            "DELETE FROM description WHERE description.id = ?",
            array($descriptionId));
    }

    public function removeProject($projectId)
    {
        return Database::queryWithAffectedRows(
            "DELETE FROM project WHERE project.id = ?",
            array($projectId));
    }

    public function getAllProjects()
    {
        return Database::getAllResults(
            'SELECT `id`, `title`, `id_main_image`, `url`
                        FROM `project`');
    }

    public function addProject($title, $mainImageId)
    {
        return Database::queryWithAffectedRows("
        INSERT INTO project (project.title, project.url, project.id_main_image)
        VALUES (?, ?, ?);
        ", array($title, $this->projectTitleToUrl($title), $mainImageId));
    }

    public function editProject($projectId, $title, $mainImageId)
    {
        if ($mainImageId == null) {
            return Database::queryWithAffectedRows("
                UPDATE project
                SET project.title = ?, project.url = ?
                WHERE project.id = ?
        ", array($title, $this->projectTitleToUrl($title), $projectId));
        } else {
            $result = Database::queryWithAffectedRows("
                UPDATE project
                SET project.title = ?, project.url = ?, project.id_main_image = ?
                WHERE project.id = ?
        ", array($title, $this->projectTitleToUrl($title), $mainImageId, $projectId));
            return $result;
        }
    }

    private function projectTitleToUrl($title)
    {
        $url = str_replace(' ', '-', $title);
        $url = iconv('UTF-8', 'ASCII//TRANSLIT', $url);
        $url = strtolower($url);
        return $url;
    }

    public function getRandomProject()
    {
        $allProjects = $this->getAllProjects();
        try {
            $randomProjectId = random_int(0, sizeof($allProjects) - 1);
            return $allProjects[$randomProjectId];
        } catch (Exception $e) {
            return $allProjects[0];
        }
    }
}