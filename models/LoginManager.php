<?php

class LoginManager
{

    public function login($username, $password) {
        $hashedPassword = hash('sha256', $password);
        $user = $this->getUser($username);

        if (!$user) {
            echo "Wrong username or password";
            return false;
        }

        if ($hashedPassword != $user['password']) {
            echo "Wrong username or password";
        }

        return $hashedPassword == $user['password'];
    }

    private function getUser($username) {
        return Database::getFirstResult("
            SELECT user.id, user.username, user.password, user.admin
            FROM user
            WHERE username = ?
        ", array($username));
    }

    public function logoutUser() {
        session_start();
        session_unset();
    }

    public function isUserLoggedIn() {
        if (isset($_SESSION['username']) && !empty($_SESSION['username'])) {
            return true;
        } else {
            return false;
        }
    }

    public function createSession($username) {
        $_SESSION['username'] = $username;
    }
}