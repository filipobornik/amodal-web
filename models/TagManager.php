<?php

class TagManager
{

    public function getTagsForDescription($descriptionId) {
        return Database::getAllResults("
        SELECT description.id AS description_id, tag.id as tag_id, tag_name
        FROM description
        JOIN projectDescription_tags ON description.id = projectDescription_tags.id_description
        JOIN tag ON tag.id = projectDescription_tags.id_tag
        WHERE description.id = ?
        ", array($descriptionId));
    }

    public function getAllTags() {
        return Database::getAllResults("
        SELECT id, tag_name
        FROM tag
        ", array());
    }

    public function editTag($tagId, $tagName) {
        return Database::queryWithAffectedRows("
        UPDATE tag
        SET tag.tag_name = ?
        WHERE tag.id = ?
        ", array($tagName, $tagId));
    }

    public function removeTagsForDescription($descriptionId) {
        return Database::queryWithAffectedRows("
        DELETE FROM projectDescription_tags WHERE projectDescription_tags.id_description = ?",
            array($descriptionId));
    }

    public function removeTag($tagId) {
        $result = Database::queryWithAffectedRows(
            "DELETE FROM projectDescription_tags WHERE projectDescription_tags.id_tag = ?",
            array($tagId));
        $result += Database::queryWithAffectedRows(
            "DELETE FROM tag WHERE tag.id = ?",
            array($tagId));
        return $result;
    }

    public function getTag($tagId) {
        return Database::getFirstResult("
        SELECT id, tag_name
        FROM tag
        WHERE id = ?
        ", array($tagId));
    }

    public function addTag($tagName) {
        return Database::queryWithAffectedRows("
        INSERT INTO tag (tag.tag_name)
        VALUES (?);
        ", array($tagName));
    }

    public function bindTagsToDescription($descriptionId, $tagIds){
        $result = 0;
        $this->removeTagsForDescription($descriptionId);
        foreach ($tagIds as $id) {
            $result += Database::queryWithAffectedRows("
            INSERT INTO projectDescription_tags 
            (projectDescription_tags.id_tag, projectDescription_tags.id_description)
            VALUES(?,?)",
                array($id, $descriptionId));
        }
        return $result;
    }
}