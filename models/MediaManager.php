<?php

class MediaManager
{

    public function getMediaForDescription($descriptionId) {
        return Database::getAllResults("
        SELECT media.id_description, media.id, media.url, media.type
        FROM description
        JOIN media ON media.id_description = description.id
        WHERE description.id = ?
        ", array($descriptionId));
    }

    public function getAllMedia() {
        return Database::getAllResults("
        SELECT media.id_description, media.id, media.url, media.type
        FROM media
        ", array());
    }

    public function removeMedia($mediaId) {
        return Database::queryWithAffectedRows(
            "DELETE FROM media WHERE media.id = ?",
            array($mediaId));
    }

    public function editMedia($mediaId, $mediaType, $mediaUrl) {
        return Database::queryWithAffectedRows("
        UPDATE media
        SET media.type = ?, media.url = ?
        WHERE media.id = ?
        ", array($mediaType, $mediaUrl, $mediaId));
    }

    public function getMedia($mediaId) {
        return Database::getFirstResult("
        SELECT media.id_description, media.id, media.url, media.type
        FROM media
        WHERE id = ?
        ", array($mediaId));
    }

    public function addMedia($mediaUrl, $mediaType, $media_id_description) {
        return Database::queryWithAffectedRows("
        INSERT INTO media (media.url, media.type, media.id_description)
        VALUES (?, ?, ?);
        ", array($mediaUrl, $mediaType, $media_id_description));
    }
}