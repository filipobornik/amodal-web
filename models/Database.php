<?php

class Database
{

    private static $connection;

    private static $settings = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        PDO::ATTR_EMULATE_PREPARES => false,
    );

    public static function connect($host, $user, $password, $database) {
        if (!isset(self::$connection)) {
            self::$connection = @new PDO(
                "mysql:host=$host;dbname=$database",
                $user,
                $password,
                self::$settings
            );
        }
    }

    public static function getFirstColumnOfFirstResult($query, $params = array()) {
        $result = self::getFirstResult($query, $params);
        return $result[0];
    }

    public static function getFirstResult($query, $params = array()) {
        $result = self::$connection->prepare($query);
        $result->execute($params);
        return $result->fetch();
    }

    public static function getAllResults($query, $params = array()) {
        $result = self::$connection->prepare($query);
        $result->execute($params);
        return $result->fetchAll();
    }

    public static function queryWithAffectedRows($query, $params = array()){
        $result = self::$connection->prepare($query);
        $result->execute($params);
        return $result->rowCount();
    }

    public static function queryWithInsertedRowId($query, $params = array()){
        $result = self::$connection->prepare($query);
        $result->execute($params);
        return self::$connection->lastInsertId();
    }

}