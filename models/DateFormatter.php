<?php

class DateFormatter
{

    public static function formatDate($databaseDate) {
        return date("m/Y", strtotime($databaseDate));
    }

}